var express = require('express');
var exphbs = require('express-handlebars');
var express_handlebars_sections = require("express-handlebars-sections");
var morgan= require('morgan');


var app = express();

require('./middlewares/session')(app);
require('./middlewares/passport')(app);


app.use(require('./middlewares/auth_local.mdw'));


app.use(morgan('dev'));
app.use(express.urlencoded());
app.use(express.json());
app.use(express.static('public'));

app.engine('hbs', exphbs({
    defaultLayout:'main.hbs',
    layoutsDir:'views/layouts',
    helpers:{
        equal(v1,v2) {
            return (v1 === v2);
        }
    }
}));
app.set('view engine','hbs');


app.get('/',(req, res) => {
    
    
    res.render('home',{data:{user:req.user,}});
})


app.use(require('./middlewares/local.mdw'));
//app.use(require('./middlewares/chuyenmuc.mdw'));
app.use(require('./middlewares/tag.mdw'));
// const hbs = exphbs.create({
//     defaultLayout:'main.hbs',
//     layoutsDir: "views/layouts"
//     // Hàm định dạng title của product khi ở fast cart
// });
// express_handlebars_sections(hbs);
// app.engine("hbs", hbs.engine);
// app.set("view engine", "hbs");


// exphbs.registerHelper('ifCond', function(v1, v2, options) {
//     if(v1 === v2) {
//       return options.fn(this);
//     }
//     return options.inverse(this);
//   });
app.get('/logout',(req,res)=>{
    req.logOut();
    res.redirect('/');
})
app.use('/PhongVien',require('./routes/PhongVien/PhongVien.route'));
app.use('/trangchu', require('./routes/Trangchu/Trangchu.route'));
app.use('/trangchuyenmuc', require('./routes/Trangchuyenmuc/Trangchuyenmuc.route'));
app.use('/editer', require('./routes/editer.route'));
app.use('/profile', require('./routes/User/profile.route'));
//app.use('/editer', require('./routes/editer.route'));
//app.use('/editer',(req,res)=>{
  //  res.render('vweidter/editer',{layout:false})
//})
app.use('/login',require('./routes/login.route'));
app.use('/register',require('./routes/register.route'));
app.use('/admin',require('./routes/admin/admin.route'));
app.use('/nonpower',(req,res)=>{
    res.render('nonPower',{layout:false,power:true});
})
//app.use('/login', require('./routes/user/user.route'));

app.use((req, res, next) => {
    res.locals.error="";
    res.locals.power=true;
    res.locals.user=req.user;
    res.render('vwError/Error', {layout: false});
}),
  
app.listen(3000, () => {
    console.log('web server is running at http://localhost:3000');
})