
module.exports=(req,res,next)=>{
    if(req.user)
    {   
        res.locals.isAuthenticalted=true;
        res.locals.authUser=req.user;
        //console.log(res);
        if(req.user.idRole==1) res.locals.roleAdmin=true;
        else if(req.user.idRole==2) res.locals.roleEditor=true;
        else if(req.user.idRole==3) res.locals.roleWriter=true;
    }
    next();
}