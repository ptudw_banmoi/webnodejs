module.exports=(req,res,next)=>{
    if(!req.user)
    {
        res.redirect('/login');
        return;
    }
    else if(req.user && req.user.idRole!=3)
    {
        res.locals.power=true;
        res.redirect('/nonpower');
        return;
    }
    else res.locals.power=false;
    next();
}