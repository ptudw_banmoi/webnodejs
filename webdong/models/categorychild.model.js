var db = require('../utils/db');
module.exports = {
    all: () => {
        return db.load('select * from categorychild;');
    },
    
    allByCategory: id=>{
        return db.load(`select * from categorychild where idCategory= ${id};`);
    },

    single: id => {
        return db.load('select * from chuyenmuc where IDChuyenMuc = ${id};');
    },

    add: entity => {
        return db.add('chuyenmuc', entity);
    },

    update: entity => {
        return db.update('chuyenmuc', 'IDChuyenMuc', entity);
    },

    delete: id => {
        return db.delete('chuyenmuc', 'IDChuyenMuc', id);
    },
};