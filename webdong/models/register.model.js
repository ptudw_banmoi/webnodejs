var db = require('../utils/db');
module.exports = {
    all: () => {
        return db.load('select * from users;');
    },
    
    allByCategoryChild: id  => {
        return db.load(`select * from  post where idCategoryChild =${id};`);
    },

    single: id => {
        return db.load(`select * from post where id = ${id};`);
    },

    add: entity => {
        return db.add('users', entity);
    },

    update: entity => {
        return db.update('baiviet', 'IDBaiViet', entity);
    },

    delete: id => {
        return db.delete('baiviet', 'IDBaiViet', id);
    },
};