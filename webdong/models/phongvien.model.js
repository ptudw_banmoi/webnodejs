var db = require('../utils/db');
module.exports = {
    all: idReporter => {
        return db.load(`select * from post where idReporter =${idReporter};`);
    },
    HC:idReporter=>{
        return db.load(`SELECT c.name as category , p.id, p.title, p.reason, s.name as status FROM status s join categorychild c join post p  on p.idReporter =${idReporter} and  p.idCategoryChild = c.id  and  p.idStatus>=3 and p.idStatus<5 and p.idStatus=s.id        group by p.id`);
    },
    
    XB:idReporter=>{
        return db.load(`SELECT c.name as category , p.id, p.title, p.reason, s.name as status FROM status s join categorychild c join post p  on p.idReporter =${idReporter} and  p.idCategoryChild = c.id  and  p.idStatus=1 and p.idStatus=s.id        group by p.id`);
    },
    
    DD:idReporter=>{
        return db.load(`SELECT c.name as category , p.id, p.title, p.reason, s.name as status FROM status s join categorychild c join post p  on p.idReporter =${idReporter} and  p.idCategoryChild = c.id  and  p.idStatus=2 and p.idStatus=s.id        group by p.id`);
    },
    
    CD:idReporter=>{
        return db.load(`SELECT c.name as category , p.id, p.title, p.reason, s.name as status FROM status s join categorychild c join post p  on p.idReporter =${idReporter} and  p.idCategoryChild = c.id  and  p.idStatus=3 and p.idStatus=s.id        group by p.id`);
    },
    
    TC:idReporter=>{
        return db.load(`SELECT c.name as category , p.id, p.title, p.reason, s.name as status FROM status s join categorychild c join post p  on p.idReporter =${idReporter} and  p.idCategoryChild = c.id  and  p.idStatus=4 and p.idStatus=s.id        group by p.id`);
    },    
    loadCategoryChild:()=>{
        return db.load('Select c.id as idCategoryChild,c.name as nameCategoryChild from categoryChild c');
    },
    loadCategoryChildPro:()=>{
        return db.load('Select cc.id as idCategoryChild,cc.name as nameCategoryChild, c.name as nameCategory,c.id as idCategory from categoryChild cc join category c where cc.idCategory=c.id group by cc.id');
    },
    loadCategory:()=>{
        return db.load('Select c.id as idCategory,c.name as nameCategory from category c');
    },
    loadTag:()=>{
        return db.load('Select t.id as idTag, t.name as nameTag  from tag t');
    },
    loadTagPost:id=>{
        return db.load(`Select idTag as idTagPost  from tagPost where idPost=${id}`);
    },
    add: entity =>{
        return db.add('post',entity);
    },
    addCategory: entity =>{
        return db.add('category',entity);
    },
    addCategoryChild: entity =>{
        return db.add('categoryChild',entity);
    },
    
    addTag: entity =>{
        return db.add('tag',entity);
    },
    update: entity =>{
        return db.update('post','id',entity);
    },
    
    updateCategory: entity =>{
        return db.update('category','id',entity);
    },
    
    updateCategoryChild: entity =>{
        return db.update('categoryChild','id',entity);
    },
    updateTag: entity =>{
        return db.update('tag','id',entity);
    },
    delete: id =>{
        return db.delete('post','id',id);
    },
    deleteTagPost: id =>{
        return db.delete('tagPost','idPost',id);
    },
    single: id =>{
        return db.load(`select * from post where id= ${id}`);
    },
    addTagPost: entity2 =>{
        return db.add('tagPost',entity2);
    }

};