var db = require("../utils/db");

module.exports={
    getAccountAll: () => {
        return db.load(`SELECT u.id,u.username,u.full_name,r.name,r.id as idRoleUser FROM users u join roleUser r where u.idRole=r.id group by u.id`);
    },
    getAccountByUsername: username => {
        return db.load(`SELECT * FROM users WHERE username=${username}`);
    },
    getAccountById: id => {
        return db.load(`SELECT * FROM users WHERE id=${id}`);
    },
    getRoleUserAll:()=>
    {
        return db.load(`SELECT * FROM roleUser`);
    },
    createAccount: entity => {
        return db.add('user',entity);
    },
    addInfo: account =>{
        return db.update('users','id',account);
    },
};