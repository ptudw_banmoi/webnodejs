var db = require('../utils/db');
module.exports = {
    all: () => {
        return db.load('select * from post;');
    },
    
    allByCategoryChild: id  => {
        return db.load(`select *, p.id as idPost from post p join users where p.idCategoryChild =${id};`);
    },

    single: id => {
        return db.load(`select *, p.id as idPost,cc.name as nameCategoryChild,c.name as nameCategory  from post p join users join categoryChild cc join category c where p.id = ${id} and p.idCategoryChild = cc.id and cc.idCategory=c.id;`);
    },

    getTop10ViewsOfCategoryChild: id => {
        return db.load(`select *,p.id as idPost from post p join users where p.idCategoryChild =${id} ORDER BY numberVisits DESC limit 10;`);
    },

    getTop10NewOfCategoryChild: id => {
        return db.load(`select *,p.id as idPost from post p join users where p.idCategoryChild =${id} ORDER BY numberVisits DESC limit 10;`);
    },

    getTop10Views : () => {
        return db.load('select *,p.id as idPost from post p join users  ORDER BY numberVisits DESC limit 10;');
    },

    getTop10New : () => {
        return db.load('select *, p.id as idPost from post p join users  ORDER BY time DESC limit 10;');
    },

    search: string => {
        return db.load(`select *,p.id as idPost from post p join users where title LIKE '%${string}%';`);
    },
};