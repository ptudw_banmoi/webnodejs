var express = require('express');
var categoryModel = require('../../models/category.model');
var postModel = require('../../models/post.model');
var categorychildModel = require('../../models/categorychild.model');
var router = express.Router();

router.get('/',(req, res, next) => {
    Promise.all([
        categoryModel.all(),
        categorychildModel.all(),
    ])
    .then(values => {
        res.render('trangbao/trangchu/trangchu', {category: values[0], categorychild: values[1]}         
        );
    }).catch(next);
})

router.get('/chuyenmuc/:id',(req, res, next) => {
    var id = req.params.id;
    Promise.all([
        categoryModel.all(),
        categorychildModel.all(),
        postModel.allByCategoryChild(id),
        //categorychildModel.allByCategory(values[0].id)
    ])  
    .then(values => {
        //console.log(rows);
        res.render('vwBaiViet/ByChuyenMuc', { layout: "main.hbs", category: values[0], categorychild: values[1], post:values[2]}       
        );
    }).catch(next);
})

router.get('/search',(req, res, next) => {
    var name = req.query.search;
    Promise.all([
        categoryModel.all(),
        categorychildModel.all(),
        postModel.search(name),
    ])  
    .then(values => {
        res.render('vwBaiViet/BySearch', { layout: "main.hbs",category: values[0], categorychild: values[1], post:values[2]}           
        );
    }).catch(next);
})

module.exports= router;