
var express = require('express');
var router = express.Router();
var passport = require('passport');
var auth = require('../middlewares/auth');
var bcrypt=require('bcrypt');
var accountModel=require('../models/account.model');
/* GET users listing. */
router.get('/', noLoggedIn, (req, res) => {
  res.render('Users/login', { layout: false, error: req.error, });
});

router.post('/', function (req, res, next) {
  passport.authenticate('local', function (err, user, info) {
    console.log(user);
    if (err) { return next(err); }
    if (!user) {
      return res.render("Users/login", { layout: false, error: info.message });
    }
    req.logIn(user, function (err) {
      if (err) { return next(err); }
      return res.redirect('/');
    });
  })(req, res, next);
});
router.get('/changepass', auth, function (req, res, next) {
  res.render("Users/changePass", { error: '' });
});
router.post('/changepass', auth, function (req, res, next) {
  var ret=bcrypt.compareSync(req.body.old_password,req.user.password);
  if (!ret) {
    return res.render("Users/changePass", {error: 'Mật khẩu sai' });
  }

  if (req.body.old_password === req.body.new_pasword) {
    return res.render("Users/changePass", {error: 'Mật khẩu mới giống mật khẩu cũ' });
  }
  if(!req.body.new_pasword || !req.body.confirm_newpass){
    return res.render("Users/changePass", { error: 'Không được để trống.' });
  }
  if (req.body.new_pasword !== req.body.confirm_newpass) {
    return res.render("Users/changePass", { error: 'Xác nhận lại mật khẩu sai' });
  }
  var salt = bcrypt.genSaltSync(10);
  var hash = bcrypt.hashSync(req.body.new_pasword, salt);
  var account = {
    id: req.user.id,
    password: hash,
  }
  accountModel.addInfo(account).then(function () {
    req.logOut();
    req.error='Đổi mật khẩu thành công, đăng nhập lại';
    res.redirect('/login')
    //res.render("Users/login", {layout:false, error: 'Đổi mật khẩu thành công, đăng nhập lại' });
  }
  ).catch(err => {
    console.log(err);
    return res.render("Users/changePass", { layout: false, error: 'Đổi mật khẩu thất bại.' });
  });
  
});

module.exports = router;
function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/');
}
function noLoggedIn(req, res, next) {
  if (!req.isAuthenticated()) {
    return next();
  }
  res.redirect('/');
}