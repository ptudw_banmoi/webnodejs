var express = require('express');
var phongvienModel = require('../../models/phongvien.model');
var router = express.Router();
var authPV= require('../../middlewares/auth-PV');
router.get('/',authPV,(req, res)=>{
    //res.render('');
    
    var p = phongvienModel.loadCategoryChild();
    var t= phongvienModel.loadTag();
    p.then(rows =>{
       // console.log(rows);
        t.then(rows2=>{
            res.render('PhongVien/PhongVien-home',{
                categoryChild: rows,
                tag:rows2
            });     
        }).catch(err=>{
            console.log(err);
        });
        
    }).catch(err=>{
        console.log(err);
    });
})
router.get('/edit/:id',authPV,(req, res)=>{
    
    var id=req.params.id;
    if(isNaN(id)){
        res.render('PhongVien/PhongVien-edit',{
           
            error:true
        });
    }
    phongvienModel.single(id).then(rows=>{
        if(rows.length>0)
        {
            
            var p = phongvienModel.loadCategoryChild();
            var t= phongvienModel.loadTag();
            
            p.then(rows2 =>{
               
                for(var i=0;i<rows2.length;i++)
                {
                    if(rows2[i].idCategoryChild==rows[0].idCategoryChild)
                    {
                        rows2[i].static="1";
                        break;
                    }
                }
                
                
                t.then(rows3=>{
                    var temp=phongvienModel.loadTagPost(id);
                    temp.then(rows4=>{
                        console.log(rows4);
                        for(var i=0;i<rows3.length;i++)
                        {
                            for(var j=0;j<rows4.length;j++)
                            {
                                if(rows3[i].idTag==rows4[j].idTagPost)
                                {
                                    rows3[i].static2="2";
                                }
                            }
                        }
                        console.log(rows3);
                        res.render('PhongVien/PhongVien-edit',{
                            categoryChild: rows2,
                            tag:rows3,
                            posts:rows[0],
                            
                            error:false
                            
                        }); 
                    }).catch(err=>{
                        console.log(err);
                        
                    });
                   
                       
                }).catch(err=>{
                    console.log(err);
                    
                });
                
            }).catch(err=>{
                console.log(err);
            }); 
        }
        else {
            res.render('PhongVien/PhongVien-edit',{
                posts:rows[0],
                error:true
            });
        }
    }).catch(err=>{
        console.log(err);
        res.end('error')
    });
    
})
router.post('/update',(req, res)=>{
    console.log(req.body);
    var entity={
        id:req.body.txtId,
        img: req.body.txtImg,
        compendious: req.body.txtDescription,
        title: req.body.txtTitle,
        content: req.body.txtContent,
        idCategoryChild: req.body.group1,
        citizie:'',
        idStatus:'3',
    }
    phongvienModel.deleteTagPost(req.body.txtId).then().catch(err=>{
        console.log(err);
    });
    phongvienModel.update(entity)
    .then(n=>{
        
        
        var temp=req.body.group2;
        temp.forEach(element => {
            //console.log(element);
            var entity2={
                idPost:req.body.txtId,
                idTag:element
            }
            phongvienModel.addTagPost(entity2).catch(err=>{
                console.log(err);
            });
        });
        
        res.redirect('/PhongVien/HC');
    
    })
    .catch(err=>{
        console.log(err);
    });
})
router.post('/delete',(req, res)=>{
    var entity={
        id:req.body.txtId,
        idStatus:'5',
    }
    phongvienModel.update(entity).then(n=>{
        res.redirect('/PhongVien/HC');
    }).catch(err=>{
        console.log(err);
    });
})

router.get('/DB',authPV,(req, res)=>{
    
    var p = phongvienModel.loadCategoryChild();
    var t= phongvienModel.loadTag();
    p.then(rows =>{
        //console.log(rows);
        t.then(rows2=>{
            res.render('PhongVien/PhongVien-home',{
                categoryChild: rows,
                tag:rows2
            });     
        }).catch(err=>{
            console.log(err);
        });
        
    }).catch(err=>{
        console.log(err);
    });
})
router.post('/DB',(req, res)=>{
    console.log(req.body);
    var entity={
        idReporter:req.user.id,
        img: req.body.txtImg,
        compendious: req.body.txtDescription,
        title: req.body.txtTitle,
        content: req.body.txtContent,
        idCategoryChild: req.body.group1,
        idStatus:'3'
    }
   
    phongvienModel.add(entity)
    .then(id=>{
        //var id=phongvienModel.loadId(req.body.title);
        console.log(id);
        var temp=req.body.group2;
        temp.forEach(element => {
            //console.log(element);
            var entity2={
                idPost:id,
                idTag:element
            }
            phongvienModel.addTagPost(entity2).catch(err=>{
                console.log(err);
            });
        });
        var p = phongvienModel.CD();
        p.then(rows =>{
        //console.log(rows);
        res.render('PhongVien/PhongVien-XB',{
            post: rows
        });
    }).catch(err=>{
        console.log(err);
    });
    })
    .catch(err=>{
        console.log(err);
    });
})

router.post('/',(req, res)=>{
    console.log(req.body);
    var entity={
        idReporter:req.user.id,
        img: req.body.txtImg,
        compendious: req.body.txtDescription,
        title: req.body.txtTitle,
        content: req.body.txtContent,
        idCategoryChild: req.body.group1,
        idStatus:'3'
    }
   
    phongvienModel.add(entity)
    .then(id=>{
        //var id=phongvienModel.loadId(req.body.title);
        console.log(id);
        var temp=req.body.group2;
        temp.forEach(element => {
            //console.log(element);
            var entity2={
                idPost:id,
                idTag:element
            }
            phongvienModel.addTagPost(entity2).catch(err=>{
                console.log(err);
            });
        });
        var p = phongvienModel.CD();
        p.then(rows =>{
        //console.log(rows);
        res.render('PhongVien/PhongVien-XB',{
            post: rows
        });
    }).catch(err=>{
        console.log(err);
    });
    })
    .catch(err=>{
        console.log(err);
    });
})
router.get('/HC',authPV,(req, res)=>{
    //res.render('');
    
    var p = phongvienModel.HC(req.user.id);
    p.then(rows =>{
        console.log(rows);
        res.render('PhongVien/PhongVien-HC',{
            post: rows
        });
    }).catch(err=>{
        console.log(err);
    });
})
router.get('/XB',authPV,(req, res)=>{
    //res.render('');
    
    var p = phongvienModel.XB(req.user.id);
    p.then(rows =>{
        console.log(rows);
        res.render('PhongVien/PhongVien-XB',{
            post: rows
        });
    }).catch(err=>{
        console.log(err);
    });
})
router.get('/DD',authPV,(req, res)=>{
    //res.render('');
    
    var p = phongvienModel.DD(req.user.id);
    p.then(rows =>{
        console.log(rows);
        res.render('PhongVien/PhongVien-XB',{
            post: rows
        });
    }).catch(err=>{
        console.log(err);
    });
})
router.get('/TC',authPV,(req, res)=>{
    //res.render('');
    
    var p = phongvienModel.TC(req.user.id);
    p.then(rows =>{
        console.log(rows);
        res.render('PhongVien/PhongVien-XB',{
            post: rows
        });
    }).catch(err=>{
        console.log(err);
    });
})
router.get('/CD',authPV,(req, res)=>{
    //res.render('');
    
    var p = phongvienModel.CD(req.user.id);
    p.then(rows =>{
        console.log(rows);
        res.render('PhongVien/PhongVien-XB',{
            post: rows
        });
    }).catch(err=>{
        console.log(err);
    });
})

module.exports= router;