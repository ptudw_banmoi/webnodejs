var express = require('express');
var accountModel = require('../../models/account.model');
var router = express.Router();
var authPV = require('../../middlewares/auth-PV');
var phongvienModel = require('../../models/phongvien.model')
router.get('/', (req, res) => {
  phongvienModel.loadCategory().then(rows => {
    var dataRender = {
      category: rows,
    }
    console.log(dataRender);
    res.render('vwAdmin/admin-chuyenmuccha', { data: dataRender });

  }).catch(err => {
    console.log(err);
  });

})
router.post('/addCategory', (req, res) => {
  var category = {
    name: req.body.new_category,
  }
  phongvienModel.loadCategory().then(rows => {
    rows.forEach(element => {
      if (element.nameCategory === req.body.new_category)
        res.redirect('/');
    });
  }).catch(err => {
    console.log(err);
  })

  phongvienModel.addCategory(category).then(id => {
    res.redirect('/admin');
  }).catch(err => {
    console.log(err);
  })

})
router.post('/editCategory/:id', (req, res) => {
  var category = {
    id: req.params.id,
    name: req.body.new_category,
  }
  phongvienModel.loadCategory().then(rows => {
    rows.forEach(element => {
      if (element.nameCategory === req.body.new_category)
        res.redirect('/');
    });
  }).catch(err => {
    console.log(err);
  })

  phongvienModel.updateCategory(category).then(n => {
    res.redirect('/admin');
  }).catch(err => {
    console.log(err);
  })

})
router.get('/categoryChild', (req, res) => {
  phongvienModel.loadCategoryChildPro().then(rows => {
    phongvienModel.loadCategory().then(rows2 => {
      
      var dataRender = {
        categoryChild: rows,
        category: rows2,
      }
      console.log(dataRender);
      res.render('vwAdmin/admin-chuyenmuccon', { data: dataRender });

    }).catch(err => {
      console.log(err);
    });

  }).catch(err => {
    console.log(err);
  });

})
router.post('/addCategoryChild', (req, res) => {
  var category = {
    name: req.body.new_category,
    idCategory: req.body.group1,
  }
  phongvienModel.loadCategoryChild().then(rows => {
    rows.forEach(element => {
      if (element.nameCategoryChild === req.body.new_category)
        res.redirect('/');
    });
  }).catch(err => {
    console.log(err);
  })

  phongvienModel.addCategoryChild(category).then(id => {
    res.redirect('/admin/categoryChild');
  }).catch(err => {
    console.log(err);
  })

})

router.post('/editCategoryChild/:id', (req, res) => {
  var category = {
    id:req.params.id,
    name: req.body.new_category,
    //idCategory: req.body.group1,
  }
  phongvienModel.loadCategoryChild().then(rows => {
    rows.forEach(element => {
      if (element.nameCategoryChild === req.body.new_category)
        res.redirect('/');
    });
  }).catch(err => {
    console.log(err);
  })

  phongvienModel.updateCategoryChild(category).then(n => {
    res.redirect('/admin/categoryChild');
  }).catch(err => {
    console.log(err);
  })
})
router.get('/tag', (req, res) => {
  phongvienModel.loadTag().then(rows => {
    var dataRender = {
      tag: rows,
    }
    console.log(dataRender);
    res.render('vwAdmin/admin-tag', { data: dataRender });

  }).catch(err => {
    console.log(err);
  });

})
router.post('/addTag', (req, res) => {
  var tag = {
    name: req.body.new_category,
  }
  phongvienModel.loadTag().then(rows => {
    rows.forEach(element => {
      if (element.nameTag === req.body.new_category)
        res.redirect('/');
    });
  }).catch(err => {
    console.log(err);
  })

  phongvienModel.addTag(tag).then(id => {
    res.redirect('/admin/tag');
  }).catch(err => {
    console.log(err);
  })

})
router.post('/editTag/:id', (req, res) => {
  var tag = {
    id: req.params.id,
    name: req.body.new_category,
  }
  phongvienModel.loadTag().then(rows => {
    rows.forEach(element => {
      if (element.nameTag === req.body.new_category)
        res.redirect('/');
    });
  }).catch(err => {
    console.log(err);
  })

  phongvienModel.updateTag(tag).then(n => {
    res.redirect('/admin/tag');
  }).catch(err => {
    console.log(err);
  })

})

router.get('/xb', (req, res) => {
  res.render('vwAdmin/admin-post-xb');

})
router.get('/draft', (req, res) => {
  res.render('vwAdmin/admin-post-draft');

})
router.get('/dsUser', (req, res) => {
  accountModel.getAccountAll().then(rows => {
    var dataRender = {
      user: rows,
    }
    console.log(dataRender);
    res.render('vwAdmin/admin-dsUser', { data: dataRender });

  }).catch(err => {
    console.log(err);
  });

})
router.get('/edit/:id', (req, res) => {
  
  accountModel.getAccountById(req.params.id).then(rows => {
    console.log(rows);
    accountModel.getRoleUserAll().then(rows2=>{
      for(var i=0;i<rows2.length;i++)
                        {
                            for(var j=0;j<rows.length;j++)
                            {
                                if(rows2[i].id==rows[j].idRole)
                                {
                                    rows2[i].static2="1";
                                }
                            }
                        }
      var dataRender = {
        user: rows[0],
        roleUser:rows2,
        edit:true,
        id:req.params.id
      }
      console.log(dataRender);
      res.render('vwAdmin/admin-dsUser-edit', { data: dataRender });
  
    })
    
  }).catch(err => {
    console.log(err);
  });

})
router.post('/editUser',(req,res)=>{
  var account = {
    id:req.body.id,
    idRole:req.body.group1,
};
// console.log(account);
accountModel.addInfo(account).then(function () {
    res.redirect('/admin/dsUser');
}
).catch(err => {
    console.log(err);
});

})
module.exports = router;