var express = require('../node_modules/express');
var router = express.Router();
var editerModel = require('../models/editer.model');
var authEditor=require('../middlewares/auth-editor');
router.get('/', (req, res) => {
  editerModel.all()
    .then(rows => {
      res.render('vweidter/editer', {
        editer: rows
      });
    }).catch(err => {
      console.log(err);
      res.end('error occured.')
    });
}),

router.get('/:id', (req, res, next) => {
  var id = req.params.id;
  var d = 0;
  editerModel.abcC(id)
    .then(rows => {
     
      // console.log(res.locals.lccategoriesChild);
      res.render('vweidter/editer', {
        editer: rows
      });
    }).catch(next)
}),
router.get('/deny/:id', (req, res) => {
  var id = req.params.id;
  console.log(id);
  editerModel.single(id).then(rows => {
    if (rows.length > 0) {
      res.render('vweidter/deny', {
        error: false,
        editer: rows[0]
      });
    } else {
      res.render('vweidter/deny', {
        error: true
      });
    }
  }).catch(err => {
    console.log(err);
    res.end('error occured.')
  })
  
}),
router.post('/capnhattrangthai', (req, res) => {

    console.log(req.body.citizie);
    editerModel.updateCitizie(req.body.citizie, req.body.ID).then(n => {
      res.redirect('/editer');
    }).catch(err => {
      console.log(err);
      res.end('error occured.')
    });
  }),


  router.get('/ok/:id', (req, res) => {
    var id = req.params.id;
    var x = editerModel.single(id);
    var p = editerModel.loadCategoryChild();
    var t= editerModel.loadTag();
    p.then(rows =>{
       // console.log(rows);
        t.then(rows2=>{
          x.then(rows3=>{
            res.render('vweidter/ok',{
                categoryChild: rows,
                tag:rows2,
                editer:rows3[0]
            });     
        }).catch(err=>{
            console.log(err);
        });
        
    }).catch(err=>{
        console.log(err);
    });
          
  }).catch(err=>{
    console.log(err);
});
  }),
  router.post('/duyetbaitc',(req, res)=>{
    console.log(req.body.group1);
    
    var entity={
        id:req.body.ID,
        idCategoryChild: req.body.group1,
        citizie:'',
        idStatus:'2',
        time:req.body.nxb
    }
    editerModel.deleteTagPost(req.body.txtId).then().catch(err=>{
        console.log(err);
    });
    editerModel.update(entity)
    .then(n=>{
        
        
        var temp=req.body.group2;
        temp.forEach(element => {
            console.log(element);
            var entity2={
                idPost:req.body.txtid,
                idTag:element
            }
            editerModel.addTagPost(entity2).catch(err=>{
                console.log(err);
            });
        });
        
        res.redirect('/editer');
    
    })
    .catch(err=>{
        console.log(err);
    });
})

  /*  router.get('/ok/:id', (req, res) => {
      var id = req.params.id;
      editerModel.update(id).then(n => {
        res.redirect('/editer')
      }).catch(err => {
        console.log(err);
        res.end('error occured.')
      });
    }),
  */
  router.get('/detail',authEditor, (req, res) => {
    res.end('m da bi xoa so');
  }),
  module.exports = router;