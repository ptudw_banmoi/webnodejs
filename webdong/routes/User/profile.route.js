var express = require('express');
var router = express.Router();
var auth = require('../../middlewares/auth');
var accountModel = require('../../models/account.model');
/* GET users listing. */

router.get('/', auth, function (req, res, next) {

    accountModel.getAccountByUsername(req.user.username).then(rows => {
        //console.log(user);
        var date=rows[0].date_of_birth;
        var temp =date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
        //console.log(date);
        var dataRender = {
            error: "",
            user: rows[0],
            date_of_birth:temp,
        }
        console.log(dataRender);
        res.render('Users/profile', { data: dataRender });
    }).catch(err => {
        console.log(err);
        res.redirect('/');
    });

});
router.post('/', function (req, res, next) {
    var account = {
        full_name: req.body.display_name,
        pseudonym: req.body.pseudonym,
        email:req.body.email,
        id:req.user.id,
        date_of_birth:req.body.date_of_birth.replace('/','-'),
    };
    // console.log(account);
    accountModel.addInfo(account).then(function () {
        res.redirect('/profile');
    }
    ).catch(err => {
        console.log(err);
    });
    //res.render('account/info_account', { data: dataRender });
});
module.exports = router;  