var express = require('express');
var router = express.Router();
var passport=require('passport');
var auth=require('../../middlewares/auth');

/* GET users listing. */
router.get('/signin',noLoggedIn, (req,res)=>{
    res.render('Users/login', { layout:false, error: "",  } );
});

router.post('/signin', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
        console.log(user);
      if (err) { return next(err); }
      if (!user) { 
          return res.render("Users/login", { layout:false, error: info.message }); 
        }
      req.logIn(user, function(err) {
        if (err) { return next(err); }
        return res.redirect('/');
      });
    })(req, res, next);
  });
  router.get('/prj',auth, function(req, res, next) {
    res.end('prj');
  });

module.exports = router;
function isLoggedIn(req,res,next)
{
    if(req.isAuthenticated())
    {
        return next();
    }
    res.redirect('/');
}
function noLoggedIn(req,res,next)
{
    if(!req.isAuthenticated())
    {
        return next();
    }
    res.redirect('/');
}