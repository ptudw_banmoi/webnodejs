var express = require('express');
var postModel = require('../../models/post.model');
var categoryModel = require('../../models/category.model');
var get6categoryModel = require('../../models/category.model');
var categorychildModel = require('../../models/categorychild.model');
var Top10PostNewModel = require('../../models/post.model');
var Top10PostViewsModel= require('../../models/post.model');
var router = express.Router();

router.get('/',(req, res, next) => {
    Promise.all([
        categoryModel.all(),
        categorychildModel.all(),
        Top10PostNewModel.getTop10New(),
        Top10PostViewsModel.getTop10Views(),
        get6categoryModel.get6Category(),
    ])
    .then(values => {
        res.render('trangbao/trangchu/trangchu', {category: values[0], categorychild: values[1], Top10PostNew:values[2], Top10PostViews:values[3], get6Category : values[4]}         
        );
    }).catch(next);
})

router.get('/chuyenmuc/:id',(req, res, next) => {
    var id = req.params.id;
    Promise.all([
        categoryModel.all(),
        categorychildModel.all(),
        postModel.allByCategoryChild(id),
    ])
    .then(values => {
        //console.log(rows);
        res.render('vwBaiViet/ByChuyenMuc', {category: values[0], categorychild: values[1], post: values[2]}           
        );
    }).catch(next);
})

router.get('/chuyenmuc/:idcm/post/:idbv',(req, res, next) => {
    var idbv = req.params.idbv;
    Promise.all([
        postModel.single(idbv),
        categoryModel.all(),
        categorychildModel.all(),
    ])  
    .then(values => {
        console.log(values[0][0]);
        res.render('vwBaiViet/ByChiTiet', { layout: "main.hbs", post: values[0][0], category: values[1], categorychild: values[2]}           
        );
    }).catch(next);
})
module.exports= router;



router.get('/Search',(req, res, next) => {
    Promise.all([
        categoryModel.all(),
        categorychildModel.all(),
    ])  
    .then(values => {
        res.render('vwBaiViet/BySearch', { layout: "main.hbs", category: values[0], categorychild: values[1]}           
        );
    }).catch(next);
})
module.exports= router;