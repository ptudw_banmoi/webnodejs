
var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt');
//var moment = require('moment');
var registerModel = require('../models/register.model');
router.get('/',(req,res,next)=>{
    res.render('Users/register',{layout: false});
})
router.post('/',(req,res,next)=>{
    var saltRounds = 10;
    var hash = bcrypt.hashSync(req.body.pass, saltRounds);
    var entity = {
        full_name: req.body.full_name,
        email:req.body.email,
        username: req.body.username,
        password: hash,
        idRole: 4,
    }
   registerModel.add(entity).then(id=>{
        res.redirect('login');
    })
})
module.exports = router;